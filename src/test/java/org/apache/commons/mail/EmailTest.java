package org.apache.commons.mail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;
import java.lang.reflect.Array;
import java.nio.charset.UnsupportedCharsetException;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by shenk on 4/24/15.
 */
public class EmailTest {

    SimpleEmail email = null;

    @Before
    public void setUp() throws Exception {
        email = new SimpleEmail();
    }

    @After
    public void tearDown() throws Exception {
        email = null;
    }

    @Test
    public void testSetDebug() throws Exception {
        boolean expected = !email.debug;
        email.setDebug(expected);
        boolean actual = email.debug;
        assertEquals("Set debug value to " + expected + " failed", expected, actual);
    }

    @Test
    public void testSetAuthentication() throws Exception {
        email.setAuthentication("USER", "PASSWORD");
        assertNotNull(email.authenticator);
    }

    @Test
    public void testSetAuthenticator() throws Exception {
        // cannot access internal representation
        DefaultAuthenticator expected = new DefaultAuthenticator("USER", "PASSWORD");
        email.setAuthenticator(expected);

        assertNotNull(email.authenticator);

        assertEquals(expected, email.authenticator);
    }

    @Test
    public void testSetCharset() throws Exception {
        String expected = "ASCII";

        email.setCharset(expected);
        assertFalse("Failed to change to default character set encoding", expected.equals(email.charset));
    }

    @Test
    public void testSetCharsetUnsupported() throws Exception {
        String expected = "11235";
        try {
            email.setCharset(expected);
            fail ("Expected UnsupportedCharsetException");
        } catch (UnsupportedCharsetException e) {
            // expected exception
        }
    }

    @Test
    public void testSetCharsetNull() throws Exception {
        String expected = null;
        try {
            email.setCharset(expected);
            fail ("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // expected exception
        }
    }

    @Test
    public void testSetContentMimeMultiPart() throws Exception {
        final MimeMultipart expected = new MimeMultipart();
        email.setContent(expected);
        assertEquals("Failed to set emailBody to provided MimeMultipart", expected, email.emailBody);
    }

    @Test
    public void testSetContentObjectAndType() throws Exception {
        String expectedContent = "input content";
        String expectedContentType = EmailConstants.TEXT_PLAIN;
        email.setContent(expectedContent, expectedContentType);
        assertEquals("Failed to set content to provided content", expectedContent, email.content);
        assertEquals("Failed to set contentType to provided contentType", expectedContentType, email.contentType);
    }

    @Test
    public void testUpdateContentTypeTextPlain() throws Exception {

        String expected = EmailConstants.TEXT_PLAIN;

        assertFalse("Content type is already the same", expected.equals(email.contentType));

        email.updateContentType(expected);

        assertFalse("Content type is not the same", expected.equals(email.contentType));
    }


    @Test
    public void testUpdateContentTypeTextHtml() throws Exception {

        String expected = "; charset=" + EmailConstants.TEXT_HTML;

        assertFalse("Content type is already the same", expected.equals(email.contentType));

        email.updateContentType(expected);

        assertFalse("Content type is not the same", expected.equals(email.contentType));
    }

    @Test
    public void testUpdateContentTypeEmpty() throws Exception {

        String expected = "";

        assertFalse("Content type is already the same", expected.equals(email.contentType));

        email.updateContentType(expected);

        assertNull("Content type is not in a valid state", email.contentType);
    }

    @Test
    public void testUpdateContentTypeNull() throws Exception {

        String expected = null;

        assertTrue("Content type is not the same", expected.equals(email.contentType));

        email.updateContentType(expected);

        assertNull("Content type is not in a valid state", email.contentType);
    }

    @Test
    public void testUpdateContentTypeTODO() throws Exception {
        // TODO: more branches to test
    }

    @Test
    public void testSetHostName() throws Exception {
        String expected = "127.0.0.1";
        email.setHostName(expected);

        assertTrue("Failed to specify the hostname", expected.equals(email.hostName));
    }

    @Test
    public void testSetTLSTrue() throws Exception {
        email.setTLS(true);

        assertTrue("Failed to set TLS state", email.tls);
        assertTrue("Failed to set TLS state", email.isTLS());
    }

    @Test
    public void testSetTLSFalse() throws Exception {
        email.setTLS(false);

        assertFalse("Failed to set TLS state", email.tls);
        assertFalse("Failed to set TLS state", email.isTLS());
    }

    @Test
    public void testSetStartTLSEnabledTrue() throws Exception {
        email.setStartTLSEnabled(true);

        assertTrue("Failed to set start enabled TLS state", email.isStartTLSEnabled());
        assertTrue("Failed to set start enabled TLS state", email.tls);
    }

    @Test
    public void testSetStartTLSEnabledFalse() throws Exception {
        email.setStartTLSEnabled(false);

        assertFalse("Failed to set start enabled TLS state", email.isStartTLSEnabled());
        assertFalse("Failed to set start enabled TLS state", email.tls);
    }

    @Test
    public void testSetStartTLSRequiredTrue() throws Exception {

        email.setStartTLSRequired(true);

        assertTrue("Failed to set start TLS required state", email.isStartTLSRequired());
    }

    @Test
    public void testSetStartTLSRequiredFalse() throws Exception {

        email.setStartTLSRequired(false);
        assertFalse("Failed to set start TLS required state", email.isStartTLSRequired());
    }

    @Test
    public void testSetSmtpPort() throws Exception {
        int expected = 5000;

        email.setSmtpPort(expected);

        assertEquals("Failed to modify the SMTP port value", Integer.toString(expected), email.smtpPort);
    }

    @Test
    public void testSetSmtpPortWithInvalidPort() throws Exception {
        int expected = -5000;
        try {
            email.setSmtpPort(expected);
            fail ("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSetSmtpPortWithInvalidHighPort() throws Exception {
        int expected = 500000;  // invalid IP Port > 65535
        try {
            email.setSmtpPort(expected);
            fail ("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSetMailSessionWithoutAuth() throws Exception {
        final Properties properties = new Properties(System.getProperties());
        Session expected = Session.getInstance(properties);
        email.setMailSession(expected);

        assertEquals(expected, email.getMailSession());
    }

    @Test
    public void testSetMailSessionWithAuthButNoUsernameAndPassword() throws Exception {
        final Properties properties = new Properties(System.getProperties());
        properties.setProperty(email.MAIL_SMTP_AUTH, "true");
        Session expected = Session.getInstance(properties);
        email.setMailSession(expected);

        assertEquals(expected, email.getMailSession());
    }

    @Test
    public void testSetMailSessionWithAuthAndUsernameAndPassword() throws Exception {
        String username = "SMTPUSERNAME";
        String password = "SMTPPASSWORD";
        final Properties properties = new Properties(System.getProperties());
        properties.setProperty(email.MAIL_SMTP_AUTH, "true");
        properties.setProperty(email.MAIL_SMTP_USER, username);
        properties.setProperty(email.MAIL_SMTP_PASSWORD, password);
        DefaultAuthenticator authenticator = new DefaultAuthenticator(username, password);
        Session in = Session.getInstance(properties);
        Session expected = Session.getInstance(in.getProperties(), authenticator);

        email.setMailSession(in);
        Session actual = email.getMailSession();

        assertNotEquals("setMailSession should create a new session if auth true and username and password provided",
                in, actual);
        assertNotNull("setMailSession should set a new default authenticator if auth true and username and password provided",
                email.authenticator);
        assertEquals("the new session should contain the properties of the input session",
                expected.getProperties(), email.getMailSession().getProperties());
    }

    @Test
    public void testSetMailSessionFromJNDINull() throws Exception {
        try {
            email.setMailSessionFromJNDI(null);
            fail ("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSetMailSessionFromJNDIWithPrefix() throws Exception {
        try {
            email.setMailSessionFromJNDI("java:DUMMYJNDINAME");
            fail("Expected NamingException");
        } catch (NamingException e) {
            // expected
        }
    }

    public void testSetMailSessionFromJNDIWithoutPrefix() throws Exception {
        try {
            email.setMailSessionFromJNDI("DUMMYJNDINAME");
            fail("Expected NamingException");
        } catch (NamingException e) {
            // expected
        }
    }

    @Test
    public void testGetMailSessionWithNoHostName() throws Exception {
        try {
            email.getMailSession();
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testGetMailSessionWithOnlyHostName() throws Exception {
        email.hostName = "50.92.63.128";

        Session actualSession = email.getMailSession();
        assertNotNull("Failed to return a Session object", actualSession);

        Properties actualProperties = actualSession.getProperties();

        assertEquals("Failed to set session mail transport protocol property to smtp",
                email.SMTP, actualProperties.getProperty(email.MAIL_TRANSPORT_PROTOCOL, null));
        assertEquals("Failed to set session mail port property to smtpPort",
                email.smtpPort, actualProperties.getProperty(email.MAIL_PORT, null));
        assertEquals("Failed to set session host name property to hostName",
                email.hostName, actualProperties.getProperty(email.MAIL_HOST, null));
        assertEquals("Failed to set session debug property to false", "false", actualProperties.getProperty(email.MAIL_DEBUG, null));
        assertEquals("Failed to set session tls enabled property to false",
                "false", actualProperties.getProperty(EmailConstants.MAIL_TRANSPORT_STARTTLS_ENABLE, null));
        assertEquals("Failed to set session tls required property to false",
                "false", actualProperties.getProperty(EmailConstants.MAIL_TRANSPORT_STARTTLS_REQUIRED, null));
        assertEquals("Failed to set session smtp send partial to false",
                "false", actualProperties.getProperty(EmailConstants.MAIL_SMTP_SEND_PARTIAL, null));
        assertEquals("Failed to set session smtps send partial to false",
                "false", actualProperties.getProperty(EmailConstants.MAIL_SMTPS_SEND_PARTIAL, null));
    }

    @Test
    public void testGetMailSessionWithHostNameAndAllProperties() throws Exception {
        email.hostName = "50.92.63.128";
        email.authenticator = new DefaultAuthenticator("USER", "PASSWORD");
        email.ssl = true;
        email.setSSLCheckServerIdentity(true);
        email.bounceAddress = "127.0.0.1";
        email.socketTimeout = 1;
        email.socketConnectionTimeout = 1;

        Session actualSession = email.getMailSession();
        assertNotNull("Failed to return a Session object", actualSession);

        Properties actualProperties = actualSession.getProperties();

        assertEquals("Failed to set session smtp auth to true",
                "true", actualProperties.getProperty(email.MAIL_SMTP_AUTH, null));
        assertEquals("Failed to set session mail port to sslSmtpPort",
                email.sslSmtpPort, actualProperties.getProperty(email.MAIL_SMTP_SOCKET_FACTORY_PORT, null));
        assertEquals("Failed to set session mail class to javax.net.ssl.SSLSocketFactory",
                "javax.net.ssl.SSLSocketFactory", actualProperties.getProperty(email.MAIL_SMTP_SOCKET_FACTORY_CLASS, null));
        assertEquals("Failed to set session mail fallback to false",
                "false", actualProperties.getProperty(email.MAIL_SMTP_SOCKET_FACTORY_FALLBACK, null));

        assertEquals("Failed to set session check server identity to true",
                "true", actualProperties.getProperty(EmailConstants.MAIL_SMTP_SSL_CHECKSERVERIDENTITY, null));

        assertEquals("Failed to set session bounce address to bounceAddress",
                email.bounceAddress, actualProperties.getProperty(email.MAIL_SMTP_FROM, null));

        assertEquals("Failed to set session socket timeout to socketTimeout",
                Integer.toString(email.socketTimeout), actualProperties.getProperty(email.MAIL_SMTP_TIMEOUT, null));

        assertEquals("Failed to set session socket connection timeout to socketConnectionTimeout",
                Integer.toString(email.socketConnectionTimeout), actualProperties.getProperty(email.MAIL_SMTP_CONNECTIONTIMEOUT, null));
    }

    @Test
    public void testSetFromEmail() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = expectedEmail;
        String expectedCharset = "UTF-16";

        email.setFrom(expectedEmail);
        assertNotNull("setFrom should set a fromAddress", email.fromAddress);
        assertEquals("setFrom should set a fromAddress with the given email",
                expectedEmail, email.fromAddress.getAddress());
        assertEquals("setFrom should set a fromAddress with the personal as the given email if no name given",
                expectedName, email.fromAddress.getPersonal());
    }

    @Test
    public void testSetFromEmailAndName() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.setFrom(expectedEmail, expectedName);
        assertNotNull("setFrom should set a fromAddress", email.fromAddress);
        assertEquals("setFrom should set a fromAddress with the given email",
                expectedEmail, email.fromAddress.getAddress());
        assertEquals("setFrom should set a fromAddress with the given name",
                expectedName, email.fromAddress.getPersonal());
    }

    @Test
    public void testSetFromEmailAndNameAndCharset() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "US-ASCII";

        email.setFrom(expectedEmail, expectedName, expectedCharset);
        assertNotNull("setFrom should set a fromAddress", email.fromAddress);
        assertEquals("setFrom should set a fromAddress with the given email",
                expectedEmail, email.fromAddress.getAddress());
        assertEquals("setFrom should set a fromAddress with the given name",
                expectedName, email.fromAddress.getPersonal());
    }

    @Test
    public void testAddToEmail() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = expectedEmail;
        String expectedCharset = "UTF-16";

        email.addTo(expectedEmail);

        assertTrue("addTo should add an InternetAddress to the toList", email.toList.size() == 1);
        InternetAddress actual = email.toList.remove(0);
        assertNotNull("addTo should add an InternetAddress to the toList", actual);

        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addTo should set an InternetAddress with the name as the given email if no name given",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddToEmailAndName() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addTo(expectedEmail, expectedName);

        assertTrue("addTo should add an InternetAddress to the toList", email.toList.size() == 1);
        InternetAddress actual = email.toList.remove(0);
        assertNotNull("addTo should add an InternetAddress to the toList", actual);

        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addTo should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddToEmailAndNameAndCharset() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "US-ASCII";

        email.addTo(expectedEmail, expectedName, expectedCharset);

        assertTrue("addTo should add an InternetAddress to the toList", email.toList.size() == 1);
        InternetAddress actual = email.toList.remove(0);
        assertNotNull("addTo should add an InternetAddress to the toList", actual);

        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addTo should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddToEmailArray() throws Exception {
        String expectedEmail1 = "test1@test.com";
        String expectedName1 = expectedEmail1;
        String expectedCharset1 = "UTF-16";

        String expectedEmail2 = "test2@test.com";
        String expectedName2 = expectedEmail2;
        String expectedCharset2 = "UTF-16";

        String expectedEmail3 = "test3@test.com";
        String expectedName3 = expectedEmail3;
        String expectedCharset3 = "UTF-16";

        String emails[] = {expectedEmail1, expectedEmail2, expectedEmail3};

        email.addTo(emails);

        assertTrue("addTo should add a list of InternetAddresses to the toList", email.toList.size() == 3);

        Iterator<InternetAddress> iter = email.toList.iterator();

        assertTrue("addTo should add an InternetAddress to the toList", iter.hasNext());
        InternetAddress actual1 = iter.next();
        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail1, actual1.getAddress());
        assertEquals("addTo should set an InternetAddress with the name as the given email if no name given",
                expectedName1, actual1.getPersonal());

        assertTrue("addTo should add an InternetAddress to the toList", iter.hasNext());
        InternetAddress actual2 = iter.next();
        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail2, actual2.getAddress());
        assertEquals("addTo should set an InternetAddress with the name as the given email if no name given",
                expectedName2, actual2.getPersonal());

        assertTrue("addTo should add an InternetAddress to the toList", iter.hasNext());
        InternetAddress actual3 = iter.next();
        assertEquals("addTo should add an InternetAddress with the given email",
                expectedEmail3, actual3.getAddress());
        assertEquals("addTo should set an InternetAddress with the name as the given email if no name given",
                expectedName3, actual3.getPersonal());
    }

    @Test
    public void testAddToEmailArrayEmpty() throws Exception {
        try {
            String emails[] = {};
            email.addTo(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testAddToEmailArrayNull() throws Exception {
        try {
            String emails[] = null;
            email.addTo(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testSetToArrayList() throws Exception {
        ArrayList<InternetAddress> expected = new ArrayList<InternetAddress>();
        InternetAddress addr1 = new InternetAddress();
        InternetAddress addr2 = new InternetAddress();
        InternetAddress addr3 = new InternetAddress();
        expected.add(addr1);
        expected.add(addr2);
        expected.add(addr3);
        email.setTo(expected);

        assertTrue("should set email's toList to a new ArrayList with the contents of the provided collection",
                expected.equals(email.toList));
    }

    @Test
    public void testSetToEmptyArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = new ArrayList<InternetAddress>();
            email.setTo(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetToNullArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = null;
            email.setTo(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testAddCcEmail() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = expectedEmail;
        String expectedCharset = "UTF-16";

        email.addCc(expectedEmail);

        assertTrue("addCc should add an InternetAddress to the ccList", email.ccList.size() == 1);
        InternetAddress actual = email.ccList.remove(0);
        assertNotNull("addCc should add an InternetAddress to the ccList", actual);

        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addCc should set an InternetAddress with the name as the given email if no name given",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddCcEmailAndName() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addCc(expectedEmail, expectedName);

        assertTrue("addCc should add an InternetAddress to the ccList", email.ccList.size() == 1);
        InternetAddress actual = email.ccList.remove(0);
        assertNotNull("addCc should add an InternetAddress to the ccList", actual);

        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addCc should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddCcEmailAndNameAndCharset() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addCc(expectedEmail, expectedName, expectedCharset);

        assertTrue("addCc should add an InternetAddress to the ccList", email.ccList.size() == 1);
        InternetAddress actual = email.ccList.remove(0);
        assertNotNull("addCc should add an InternetAddress to the ccList", actual);

        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addCc should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddCcEmailArray() throws Exception {
        String expectedEmail1 = "test1@test.com";
        String expectedName1 = expectedEmail1;
        String expectedCharset1 = "UTF-16";

        String expectedEmail2 = "test2@test.com";
        String expectedName2 = expectedEmail2;
        String expectedCharset2 = "UTF-16";

        String expectedEmail3 = "test3@test.com";
        String expectedName3 = expectedEmail3;
        String expectedCharset3 = "UTF-16";

        String emails[] = {expectedEmail1, expectedEmail2, expectedEmail3};

        email.addCc(emails);

        assertTrue("addCc should add a list of InternetAddresses to the ccList", email.ccList.size() == 3);

        Iterator<InternetAddress> iter = email.ccList.iterator();

        assertTrue("addCc should add an InternetAddress to the ccList", iter.hasNext());
        InternetAddress actual1 = iter.next();
        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail1, actual1.getAddress());
        assertEquals("addCc should set an InternetAddress with the name as the given email if no name given",
                expectedName1, actual1.getPersonal());

        assertTrue("addCc should add an InternetAddress to the ccList", iter.hasNext());
        InternetAddress actual2 = iter.next();
        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail2, actual2.getAddress());
        assertEquals("addCc should set an InternetAddress with the name as the given email if no name given",
                expectedName2, actual2.getPersonal());

        assertTrue("addCc should add an InternetAddress to the ccList", iter.hasNext());
        InternetAddress actual3 = iter.next();
        assertEquals("addCc should add an InternetAddress with the given email",
                expectedEmail3, actual3.getAddress());
        assertEquals("addCc should set an InternetAddress with the name as the given email if no name given",
                expectedName3, actual3.getPersonal());
    }

    @Test
    public void testAddCcEmailArrayEmpty() throws Exception {
        try {
            String emails[] = {};
            email.addCc(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testAddCcEmailArrayNull() throws Exception {
        try {
            String emails[] = null;
            email.addCc(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testSetCc() throws Exception {
        ArrayList<InternetAddress> expected = new ArrayList<InternetAddress>();
        InternetAddress addr1 = new InternetAddress();
        InternetAddress addr2 = new InternetAddress();
        InternetAddress addr3 = new InternetAddress();
        expected.add(addr1);
        expected.add(addr2);
        expected.add(addr3);
        email.setCc(expected);

        assertTrue("should set email's ccList to a new ArrayList with the contents of the provided collection",
                expected.equals(email.ccList));
    }

    @Test
    public void testSetCcEmptyArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = new ArrayList<InternetAddress>();
            email.setCc(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetCcNullArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = new ArrayList<InternetAddress>();
            email.setCc(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testAddBccEmail() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = expectedEmail;
        String expectedCharset = "UTF-16";

        email.addBcc(expectedEmail);

        assertTrue("addBcc should add an InternetAddress to the BccList", email.bccList.size() == 1);
        InternetAddress actual = email.bccList.remove(0);
        assertNotNull("addBcc should add an InternetAddress to the BccList", actual);

        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addBcc should set an InternetAddress with the name as the given email if no name given",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddBccEmailAndName() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addBcc(expectedEmail, expectedName);

        assertTrue("addBcc should add an InternetAddress to the BccList", email.bccList.size() == 1);
        InternetAddress actual = email.bccList.remove(0);
        assertNotNull("addBcc should add an InternetAddress to the BccList", actual);

        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addBcc should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddBccEmailAndNameAndCharset() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addBcc(expectedEmail, expectedName, expectedCharset);

        assertTrue("addBcc should add an InternetAddress to the BccList", email.bccList.size() == 1);
        InternetAddress actual = email.bccList.remove(0);
        assertNotNull("addBcc should add an InternetAddress to the BccList", actual);

        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addBcc should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddBccEmailList() throws Exception {
        String expectedEmail1 = "test1@test.com";
        String expectedName1 = expectedEmail1;
        String expectedCharset1 = "UTF-16";

        String expectedEmail2 = "test2@test.com";
        String expectedName2 = expectedEmail2;
        String expectedCharset2 = "UTF-16";

        String expectedEmail3 = "test3@test.com";
        String expectedName3 = expectedEmail3;
        String expectedCharset3 = "UTF-16";

        String emails[] = {expectedEmail1, expectedEmail2, expectedEmail3};

        email.addBcc(emails);

        assertTrue("addBcc should add a list of InternetAddresses to the BccList", email.bccList.size() == 3);

        Iterator<InternetAddress> iter = email.bccList.iterator();

        assertTrue("addBcc should add an InternetAddress to the BccList", iter.hasNext());
        InternetAddress actual1 = iter.next();
        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail1, actual1.getAddress());
        assertEquals("addBcc should set an InternetAddress with the name as the given email if no name given",
                expectedName1, actual1.getPersonal());

        assertTrue("addBcc should add an InternetAddress to the BccList", iter.hasNext());
        InternetAddress actual2 = iter.next();
        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail2, actual2.getAddress());
        assertEquals("addBcc should set an InternetAddress with the name as the given email if no name given",
                expectedName2, actual2.getPersonal());

        assertTrue("addBcc should add an InternetAddress to the BccList", iter.hasNext());
        InternetAddress actual3 = iter.next();
        assertEquals("addBcc should add an InternetAddress with the given email",
                expectedEmail3, actual3.getAddress());
        assertEquals("addBcc should set an InternetAddress with the name as the given email if no name given",
                expectedName3, actual3.getPersonal());
    }

    @Test
    public void testAddBccEmailListEmpty() throws Exception {
        try {
            String emails[] = {};
            email.addBcc(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testAddBccEmailListNull() throws Exception {
        try {
            String emails[] = null;
            email.addBcc(emails);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected exception
        }
    }

    @Test
    public void testSetBccArrayList() throws Exception {
        ArrayList<InternetAddress> expected = new ArrayList<InternetAddress>();
        InternetAddress addr1 = new InternetAddress();
        InternetAddress addr2 = new InternetAddress();
        InternetAddress addr3 = new InternetAddress();
        expected.add(addr1);
        expected.add(addr2);
        expected.add(addr3);
        email.setBcc(expected);

        assertTrue("should set email's bccList to a new ArrayList with the contents of the provided collection",
                expected.equals(email.bccList));
    }

    @Test
    public void testSetBccEmptyArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = new ArrayList<InternetAddress>();
            email.setBcc(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetBccNullArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = null;
            email.setBcc(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testAddReplyToEmail() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = expectedEmail;
        String expectedCharset = "UTF-16";

        email.addReplyTo(expectedEmail);

        assertTrue("addReplyTo should add an InternetAddress to the replyList", email.replyList.size() == 1);
        InternetAddress actual = email.replyList.remove(0);
        assertNotNull("addReplyTo should add an InternetAddress to the replyList", actual);

        assertEquals("addReplyTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addReplyTo should set an InternetAddress with the name as the given email if no name given",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddReplyToEmailAndName() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addReplyTo(expectedEmail, expectedName);

        assertTrue("addReplyTo should add an InternetAddress to the replyList", email.replyList.size() == 1);
        InternetAddress actual = email.replyList.remove(0);
        assertNotNull("addReplyTo should add an InternetAddress to the replyList", actual);

        assertEquals("addReplyTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addReplyTo should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testAddReplyToEmailAndNameAndCharset() throws Exception {
        String expectedEmail = "test@test.com";
        String expectedName = "Abraham Lincoln";
        String expectedCharset = "UTF-16";

        email.addReplyTo(expectedEmail, expectedName, expectedCharset);

        assertTrue("addReplyTo should add an InternetAddress to the replyList", email.replyList.size() == 1);
        InternetAddress actual = email.replyList.remove(0);
        assertNotNull("addReplyTo should add an InternetAddress to the replyList", actual);

        assertEquals("addReplyTo should add an InternetAddress with the given email",
                expectedEmail, actual.getAddress());
        assertEquals("addReplyTo should set an InternetAddress with the given name",
                expectedName, actual.getPersonal());
    }

    @Test
    public void testSetReplyToArrayList() throws Exception {
        ArrayList<InternetAddress> expected = new ArrayList<InternetAddress>();
        InternetAddress addr1 = new InternetAddress();
        InternetAddress addr2 = new InternetAddress();
        InternetAddress addr3 = new InternetAddress();
        expected.add(addr1);
        expected.add(addr2);
        expected.add(addr3);
        email.setReplyTo(expected);

        assertTrue("should set email's replyList to a new ArrayList with the contents of the provided collection",
                expected.equals(email.replyList));
    }

    @Test
    public void testSetReplyToEmptyArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = new ArrayList<InternetAddress>();
            email.setReplyTo(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetReplyToNullArrayList() throws Exception {
        try {
            ArrayList<InternetAddress> emailList = null;
            email.setReplyTo(emailList);
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetHeaders() throws Exception {
        String expectedHeaderName1 = "This is a name1";
        String expectedHeaderValue1 = "This is a value1";
        String expectedHeaderName2 = "This is a name2";
        String expectedHeaderValue2 = "This is a value2";
        String expectedHeaderName3 = "This is a name3";
        String expectedHeaderValue3 = "This is a value3";
        Map<String, String> expectedMap = new HashMap<String, String>();
        expectedMap.put(expectedHeaderName1, expectedHeaderValue1);
        expectedMap.put(expectedHeaderName2, expectedHeaderValue2);
        expectedMap.put(expectedHeaderName3, expectedHeaderValue3);

        email.setHeaders(expectedMap);

        assertTrue("setHeaders should set email's headers map to the contents of the provided map",
                expectedMap.equals(email.headers));
    }

    @Test
    public void testAddHeader() throws Exception {
        String expectedHeaderName = "This is a name";
        String expectedHeaderValue = "This is a value";

        email.addHeader(expectedHeaderName, expectedHeaderValue);

        String actualHeaderValue = email.headers.get(expectedHeaderName);
        assertTrue("Failed to add header to email's headers",
                expectedHeaderValue.equals(actualHeaderValue));
    }

    @Test
    public void testAddHeaderEmptyName() throws Exception {
        String name = "";
        String value = "This is a value";
        try {
            email.addHeader(name, value);
            fail("should fail if name is empty");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testAddHeaderEmptyValue() throws Exception {
        String name = "This is a name";
        String value = "";
        try {
            email.addHeader(name, value);
            fail("should fail if value is empty");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSetSubject() throws Exception {
        String expected = "This is the subject";

        email.setSubject(expected);

        assertEquals("should set email's subject to aSubject", expected, email.getSubject());
    }

    @Test
    public void testSetBounceAddress() throws Exception {
        String expected = "127.0.0.1";

        email.setBounceAddress(expected);
        assertEquals(expected, email.bounceAddress);
    }

    @Test
    public void testBuildMimeMessageExistingMessage() throws Exception {
        email.hostName = "50.92.63.128";
        Properties properties = new Properties();
        Session session = Session.getInstance(properties);
        email.message = new MimeMessage(session);
        try {
            email.buildMimeMessage();
            fail ("Expected IllegalStateException");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testBuildMimeMessageFailIfNoFromAddress() throws Exception {
        email.hostName = "50.92.63.128";
        try {
            email.buildMimeMessage();
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testBuildMimeMessageFailIfNoReceivers() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        try {
            email.buildMimeMessage();
            fail ("Expected EmailException");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testBuildMimeMessageSetSubjectWithCharset() throws Exception {
        email.hostName = "50.92.63.128";
        email.subject = "Not empty subject";
        email.charset = "US-ASCII        ";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        email.buildMimeMessage();
        assertEquals("Failed to set MimeMessage subject to email's subject",
                email.subject, email.message.getSubject());
    }

    @Test
    public void testBuildMimeMessageSetSubjectWithoutCharset() throws Exception {
        email.hostName = "50.92.63.128";
        email.subject = "Not empty subject";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        email.buildMimeMessage();
        assertEquals("Failed to set MimeMessage subject to the provided subject",
                email.subject, email.message.getSubject());
    }

    @Test
    public void testBuildMimeMessageSetFromAddress() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        email.buildMimeMessage();
        InternetAddress actualFrom = (InternetAddress) email.message.getFrom()[0];
        assertEquals("Failed to set MimeMessage From address to email's fromAddress",
                email.getFromAddress(), actualFrom);
    }

    @Test
    public void testBuildMimeMessageSetContentPlaintext() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        String expectedContent = "Content text";
        String expectedContentType = EmailConstants.TEXT_PLAIN;
        email.setContent(expectedContent, expectedContentType);

        email.buildMimeMessage();
        assertEquals("Failed to set MimeMessage content to email content",
                expectedContent, email.message.getContent());
        assertEquals("Failed to set MimeMessage content type to email content type",
                expectedContentType, email.message.getContentType());
    }

    @Test
    public void testBuildMimeMessageSetContentHTML() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        String expectedContent = "<html><body><pre>Content</pre></body></html>";
        String expectedContentType = EmailConstants.TEXT_HTML;
        email.setContent(expectedContent, expectedContentType);

        email.buildMimeMessage();

        assertEquals("Failed to set MimeMessage content to email content",
                expectedContent, email.message.getContent());
        assertEquals("Failed to set MimeMessage content type to email content type",
                expectedContentType, email.message.getContentType());
    }

    @Test
    public void testBuildMimeMessageSetBody() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        // body set with setcontent (multipart)
        MimeMultipart expected = new MimeMultipart();
        email.setContent(expected);

        email.buildMimeMessage();

        assertEquals("Failed to set MimeMessage content to email body",
                expected, email.message.getContent());
    }

    @Test
    public void testBuildMimeMessageSetEmptyBody() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        email.buildMimeMessage();
        assertEquals("Failed to set MimeMessage content to an empty string", "", email.message.getContent());
    }

    @Test
    public void testBuildMimeMessageSetToListOneRecipient() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.TO);
        assertTrue("Failed to set MimeMessage TO recipients to email's toList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetToListTwoRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver1@receiver1.com");
        email.addTo("receiver2@receiver2.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.TO);
        assertTrue("Failed to set MimeMessage TO recipients to email's toList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetToListThreeRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver1@receiver1.com");
        email.addTo("receiver2@receiver2.com");
        email.addTo("receiver3@receiver3.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.TO);
        assertTrue("Failed to set MimeMessage TO recipients to email's toList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetCcListOneRecipient() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addCc("receiver@receiver.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getCcAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.CC);
        assertTrue("Failed to set MimeMessage CC recipients to email's ccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetCcListTwoRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addCc("receiver1@receiver1.com");
        email.addCc("receiver2@receiver2.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getCcAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.CC);
        assertTrue("Failed to set MimeMessage CC recipients to email's ccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetCcListThreeRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addCc("receiver1@receiver1.com");
        email.addCc("receiver2@receiver2.com");
        email.addCc("receiver3@receiver3.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getCcAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.CC);
        assertTrue("Failed to set MimeMessage CC recipients to email's ccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetBccListOneRecipient() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addBcc("receiver@receiver.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getBccAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.BCC);
        assertTrue("Failed to set MimeMessage BCC recipients to email's bccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetBccListTwoRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addBcc("receiver1@receiver1.com");
        email.addBcc("receiver2@receiver2.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getBccAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.BCC);
        assertTrue("Failed to set MimeMessage BCC recipients to email's bccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetBccListThreeRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addBcc("receiver1@receiver1.com");
        email.addBcc("receiver2@receiver2.com");
        email.addBcc("receiver3@receiver3.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getBccAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getRecipients(Message.RecipientType.BCC);
        assertTrue("Failed to set MimeMessage BCC recipients to email's bccList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetReplyListOneRecipient() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.addReplyTo("reply@reply.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getReplyToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getReplyTo();
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetReplyListTwoRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.addReplyTo("reply1@reply1.com");
        email.addReplyTo("reply2@reply2.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getReplyToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getReplyTo();
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetReplyListThreeRecipients() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.addReplyTo("reply1@reply1.com");
        email.addReplyTo("reply2@reply2.com");
        email.addReplyTo("reply3@reply3.com");

        email.buildMimeMessage();
        List<InternetAddress> expectedList = email.getReplyToAddresses();
        InternetAddress[] expected = expectedList.toArray(new InternetAddress[expectedList.size()]);
        InternetAddress[] actual = (InternetAddress[]) email.message.getReplyTo();
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                Arrays.equals(expected, actual));
    }

    @Test
    public void testBuildMimeMessageSetHeadersOneEntry() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        String expectedHeaderName = "Header name";
        String expectedHeaderValue = "Header value";
        email.addHeader(expectedHeaderName, expectedHeaderValue);

        email.buildMimeMessage();
        String actualHeaderValue = email.message.getHeader(expectedHeaderName, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue.equals(actualHeaderValue));
    }

    @Test
    public void testBuildMimeMessageSetHeadersTwoEntries() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        String expectedHeaderName1 = "Header name1";
        String expectedHeaderValue1 = "Header value1";
        email.addHeader(expectedHeaderName1, expectedHeaderValue1);
        String expectedHeaderName2 = "Header name2";
        String expectedHeaderValue2 = "Header value2";
        email.addHeader(expectedHeaderName2, expectedHeaderValue2);

        email.buildMimeMessage();
        String actualHeaderValue1 = email.message.getHeader(expectedHeaderName1, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue1.equals(actualHeaderValue1));
        String actualHeaderValue2 = email.message.getHeader(expectedHeaderName2, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue2.equals(actualHeaderValue2));
    }

    @Test
    public void testBuildMimeMessageSetHeadersThreeEntries() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        String expectedHeaderName1 = "Header name1";
        String expectedHeaderValue1 = "Header value1";
        email.addHeader(expectedHeaderName1, expectedHeaderValue1);
        String expectedHeaderName2 = "Header name2";
        String expectedHeaderValue2 = "Header value2";
        email.addHeader(expectedHeaderName2, expectedHeaderValue2);
        String expectedHeaderName3 = "Header name2";
        String expectedHeaderValue3 = "Header value2";
        email.addHeader(expectedHeaderName3, expectedHeaderValue3);

        email.buildMimeMessage();
        String actualHeaderValue1 = email.message.getHeader(expectedHeaderName1, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue1.equals(actualHeaderValue1));
        String actualHeaderValue2 = email.message.getHeader(expectedHeaderName2, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue2.equals(actualHeaderValue2));
        String actualHeaderValue3 = email.message.getHeader(expectedHeaderName3, "");
        assertTrue("Failed to set MimeMessage ReplyTo members to email's ReplyList",
                expectedHeaderValue3.equals(actualHeaderValue3));
    }

    @Test
    public void testBuildMimeMessageLogIntoPop() throws Exception {
        fail("This is a slow test, comment me for final logs.");
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.setPopBeforeSmtp(true, "50.92.63.128", "BOGUSUSERNAME", "BOGUSPASSWORD");

        try {
            email.buildMimeMessage();
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSendMimeMessageNoMessage() throws Exception {
        try {
            email.sendMimeMessage();
            fail("should throw an IllegalArgumentException if the MimeMessage has not been created");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSendMimeMessageSendFail() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.buildMimeMessage();

        try {
            email.sendMimeMessage();
            fail("should throw an EmailException if the sending failed");
        } catch (EmailException e) {
            // expected
        }

    }

    @Test
    public void testGetMimeMessage() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.buildMimeMessage();
        MimeMessage expected = email.message;
        MimeMessage actual = email.getMimeMessage();

        assertEquals("should return email's MimeMessage", expected, actual);
    }

    @Test
    public void testSendBuildFail() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");
        email.buildMimeMessage();

        try {
            email.send();
            fail("should throw an IllegalStateException if the MimeMessage was already built");
        }
        catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testSendSendFail() throws Exception {
        email.hostName = "50.92.63.128";
        email.setFrom("from@from.com");
        email.addTo("receiver@receiver.com");

        try {
            email.send();
            fail("should throw an EmailException if the sending failed");
        } catch (EmailException e) {
            // expected
        }
    }

    @Test
    public void testSetSentDate() throws Exception {
        java.util.Date expected = new java.util.Date();

        email.setSentDate(expected);

        java.util.Date actual = email.sentDate;

        assertEquals (expected, actual);
        assertEquals (expected, email.getSentDate());
    }

    @Test
    public void testSetSentDateNull() throws Exception {
        java.util.Date expected = null;

        email.setSentDate(expected);

        java.util.Date actual = email.sentDate;

        assertEquals (expected, actual);
        assertNotEquals(expected, email.getSentDate());
    }

    @Test
    public void testGetSentDate() throws Exception {

        java.util.Date date = email.getSentDate();

        assertNotNull(date);
    }

    @Test
    public void testGetSubject() throws Exception {

        assertNull(email.getSubject());
    }

    @Test
    public void testGetFromAddress() throws Exception {

        assertNull(email.getFromAddress());
    }

    @Test
    public void testGetHostName() throws Exception {

        String expected = "10.10.10.10";
        email.setHostName(expected);

        String actual = email.getHostName();
        assertEquals("Failed to get the host name", expected, actual);
    }

    @Test
    public void testGetSmtpPort() throws Exception {

        int expected = 1000;
        email.setSmtpPort(1000);
        String actual = email.getSmtpPort();
        assertEquals ("Failed to get the SMTP port", Integer.toString(expected), actual);
    }

    @Test
    public void testIsStartTLSRequired() throws Exception {
        email.setStartTLSRequired(true);

        assertEquals("", true, email.isStartTLSRequired());
    }

    @Test
    public void testIsStartTLSEnabled() throws Exception {
        email.setStartTLSEnabled(true);

        assertEquals("", true, email.isStartTLSEnabled());
    }

    @Test
    public void testIsTLS() throws Exception {
        boolean expected = false;

        boolean actual = email.isTLS();

        assertEquals ("Default value for isTLS is incorrect", expected, actual);
    }

    @Test
    public void testToInternetAddressArray() throws Exception {

    }

    @Test
    public void testSetPopBeforeSmtp() throws Exception {
        final boolean expectedNewPopBeforeSmtp = true;
        final String expectedNewPopHost = "10.10.10.10";
        final String expectedNewPopUsername = "Pop username";
        final String expectedNewPopPassword = "Pop password";

        email.setPopBeforeSmtp(expectedNewPopBeforeSmtp,
                expectedNewPopHost,
                expectedNewPopUsername,
                expectedNewPopPassword);

        assertEquals("should set email's popBeforeSmtp to newPopBeforeSmtp",
                expectedNewPopBeforeSmtp, email.popBeforeSmtp);
        assertEquals("should set email's popHost to newPopHost",
                expectedNewPopHost, email.popHost);
        assertEquals("should set email's popUsername to popUsername",
                expectedNewPopUsername, email.popUsername);
        assertEquals("should set email's popPassword to popPassword",
                expectedNewPopPassword, email.popPassword);
    }

    @Test
    public void testIsSSL() throws Exception {
        boolean expected = false;

        boolean actual = email.isSSL();

        assertEquals ("Invalid state value for SSL", expected, actual);
    }

    @Test
    public void testIsSSLOnConnect() throws Exception {
        boolean expectedSSLOnConnect = true;
        email.setSSLOnConnect(expectedSSLOnConnect);

        assertEquals("should return the value of email's sslOnConnect",
                expectedSSLOnConnect, email.isSSLOnConnect());
    }

    @Test
    public void testSetSSLTrue() throws Exception {
        boolean expected = true;
        email.setSSL(expected);

        assertEquals("Failed to set SSL state", expected, email.ssl);
        assertEquals ("Failed to set ssl state", expected, email.isSSLOnConnect());
    }

    @Test
    public void testSetSSLFalse() throws Exception {
        boolean expected = false;
        email.setSSL(expected);

        assertEquals("Failed to set SSL state", expected, email.ssl);
        assertEquals ("Failed to set ssl state", expected, email.isSSLOnConnect());
    }

    @Test
    public void testSetSSLOnConnectTrue() throws Exception {
        boolean expected = false;
        email.setSSLOnConnect(expected);

        assertEquals("Failed to set SSL connect state", expected, email.tls);
        assertEquals ("Failed to set ssl connect state", expected, email.isSSLOnConnect());

    }

    @Test
    public void testIsSSLCheckServerIdentity() throws Exception {
        boolean expected = false;
        boolean actual = email.isSSLCheckServerIdentity();

        assertEquals (expected, actual);
    }

    @Test
    public void testSetSSLCheckServerIdentity() throws Exception {
        boolean expected = !email.isSSLCheckServerIdentity();
        boolean actual = email.setSSLCheckServerIdentity(expected).isSSLCheckServerIdentity();

        assertEquals (expected, actual);
    }

    @Test
    public void testGetSslSmtpPort() throws Exception {
        String expected = "465";
        email.sslSmtpPort = expected;

        String actual = email.getSslSmtpPort();

        assertEquals (expected, actual);
    }

    @Test
    public void testSetSslSmtpPort() throws Exception {
        String expected = "2001";
        email.setSslSmtpPort(expected);
        String actual = email.sslSmtpPort;

        assertEquals (expected, actual);
    }

    @Test
    public void testIsSendPartial() throws Exception {
        boolean expected = false;

        boolean actual = email.isSendPartial();

        assertEquals (expected, actual);
    }

    @Test
    public void testSetSendPartialTrue() throws Exception {
        boolean expected  = true;

        email.setSendPartial(expected);
        boolean actual = email.isSendPartial();
        assertEquals (expected, actual);
    }

    @Test
    public void testSetSendPartialFalse() throws Exception {
        boolean expected  = false;

        email.setSendPartial(expected);
        boolean actual = email.isSendPartial();
        assertEquals (expected, actual);
    }

    @Test
    public void testGetToAddresses() throws Exception {
        List<InternetAddress> toAddr = email.getToAddresses();

        assertNotNull(toAddr);
    }

    @Test
    public void testGetCcAddresses() throws Exception {
        List<InternetAddress> ccToAddr = email.getCcAddresses();

        assertNotNull(ccToAddr);
    }

    @Test
    public void testGetBccAddresses() throws Exception {
        List<InternetAddress> bccToAddr = email.getBccAddresses();

        assertNotNull(bccToAddr);
    }

    @Test
    public void testGetReplyToAddresses() throws Exception {

        List<InternetAddress> replyToAddr = email.getReplyToAddresses();

        assertNotNull(replyToAddr);
    }

    @Test
    public void testGetSocketConnectionTimeout() throws Exception {
        int defaultSocketTimeout = 60000;

        assertEquals("Socket connection timeout initial value is invalid", defaultSocketTimeout, email.socketConnectionTimeout);

        assertEquals("Socket connection timeout initial value is invalid", defaultSocketTimeout, email.getSocketConnectionTimeout());
    }

    @Test
    public void testSetSocketConnectionTimeout() throws Exception {
        int socketTimeout = 1000;
        email.setSocketConnectionTimeout(socketTimeout);

        assertEquals("Failed to change socket connection timeout value", socketTimeout, email.socketConnectionTimeout);
    }

    @Test
    public void testGetSocketTimeout() throws Exception {

        int defaultSocketTimeout = 60000;

        assertEquals("Socket timeout initial value is invalid", defaultSocketTimeout, email.socketTimeout);

        assertEquals("Socket timeout initial value is invalid", defaultSocketTimeout, email.getSocketTimeout());
    }

    @Test
    public void testSetSocketTimeout() throws Exception {
        int socketTimeout = 1000;
        email.setSocketTimeout(socketTimeout);

        assertEquals("Failed to change socket timeout value", socketTimeout, email.socketTimeout);
    }

    @Test
    public void testCreateMimeMessage() throws Exception {
        final Properties properties = new Properties(System.getProperties());
        Session session = Session.getInstance(properties);
        MimeMessage expected = new MimeMessage(session);
        MimeMessage actual = email.createMimeMessage(session);

        assertEquals("should create a MimeMessage from aSession", expected.getSession(), actual.getSession());
    }
}