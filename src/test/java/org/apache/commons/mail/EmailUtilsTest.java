package org.apache.commons.mail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Created by shenk on 4/24/15.
 */
public class EmailUtilsTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testIsEmpty() throws Exception {
        assertEquals(true, EmailUtils.isEmpty(null));
        assertEquals(true, EmailUtils.isEmpty(""));
        assertEquals(false, EmailUtils.isEmpty("testempty"));
        assertEquals(false, EmailUtils.isEmpty("teststring"));
    }

    @Test
    public void testIsNotEmpty() throws Exception {
        assertEquals(false, EmailUtils.isNotEmpty(null));
        assertEquals(false, EmailUtils.isNotEmpty(""));
        assertEquals(true, EmailUtils.isNotEmpty("testempty"));
        assertEquals(true, EmailUtils.isNotEmpty("teststring"));
        assertEquals(true, EmailUtils.isNotEmpty("teststring2"));
    }

    @Test
    public void testNotNull() throws Exception {
        try {
            EmailUtils.notNull(null, "isnull");
            fail("Expected IllegalArgumentException");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testRandomAlphabetic() throws Exception {
        try {
            EmailUtils.randomAlphabetic(-1);
            fail("Expected IllegalArgumentException");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
        assertEquals("", EmailUtils.randomAlphabetic(0));
        assertNotNull(EmailUtils.randomAlphabetic(1));
        assertNotNull(EmailUtils.randomAlphabetic(10));
    }

    @Test
    public void testEncodeUrl() throws Exception {
        assertEquals(null, EmailUtils.encodeUrl(null));
        assertNotNull(EmailUtils.encodeUrl(""));
        assertNotNull(EmailUtils.encodeUrl("ENCODEencode%"));
        assertNotNull(EmailUtils.encodeUrl("ENCODEencode"));
    }

    @Test
    public void testWriteMimeMessage() throws Exception {
        File testfile = new File("./testfile");
        final Properties properties = new Properties(System.getProperties());
        Session session = Session.getInstance(properties);
        MimeMessage message = new MimeMessage(session);

        try {
            EmailUtils.writeMimeMessage(testfile,null);
            fail("Expected NullPointerException");
        }
        catch (NullPointerException e) {
            // expected
        }
    }

    @Test
    public void testWriteValidMimeMessage() throws Exception {
        File testfile = new File("./testfile");
        final Properties properties = new Properties(System.getProperties());
        Session session = Session.getInstance(properties);
        MimeMessage message = new MimeMessage(session);
        //message = 5;
        try {
            EmailUtils.writeMimeMessage(testfile,message);
            fail("Expected IllegalArgumentException");
        }
        catch (IOException e) {
            // expected
        }
        catch (MessagingException e ) {
            // expected
        }
    }
}